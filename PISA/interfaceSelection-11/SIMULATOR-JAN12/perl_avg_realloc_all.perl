#!/usr/bin/perl

open OUTPUT, ">reallocs_avg_global";

my @hash_avg_reallocs = ();

my @files =<*>;

my $count = 0;

foreach my $file (@files)
{
	print $file . "\n";
	if ($file=~m/(14.*)/)
	{
		print "FOLDER $1\n";
		my @folder = <$1/*>;
		
		foreach my $fol (@folder)
		{
			print $fol . "\n";
		}

		open FILE, "$1/reallocs_all";
		$count ++;

		my $index = 0;

		foreach my $line (<FILE>)
		{
			print $line;
			my @split_line = split ('\t', $line);

			if ($split_line[1]==10)
			{
				$index ++;
				$hash_avg_reallocs{$index}{"1SAW"} += $split_line[2];
				$hash_avg_reallocs{$index}{"2MEW"} += $split_line[3];
				$hash_avg_reallocs{$index}{"3TOPSIS"} += $split_line[4];
				$hash_avg_reallocs{$index}{"4SAW_ITER"} += $split_line[5];
				$hash_avg_reallocs{$index}{"5MEW_ITER"} += $split_line[6];
				$hash_avg_reallocs{$index}{"6TOPSIS_ITER"} += $split_line[7];
				$hash_avg_reallocs{$index}{"7GAP"} += $split_line[8];

			}

			$hash_avg_reallocs{$index}{$split_line[1]} += $split_line[9];
			
		}
	}
}

foreach my $ind (sort {$a<=>$b} keys %hash_avg_reallocs)
{
	print "$ind";	
	print OUTPUT "$ind";		

	foreach my $gen (sort {$a<=>$b} keys %{$hash_avg_reallocs{$ind}})
	{
		my $ratio = $hash_avg_reallocs{$ind}{$gen} / $count;
		print "	$gen	$ratio";	
		print OUTPUT "	$gen	$ratio";	
	}
	
	print "\n";
	print OUTPUT "\n";
}
