#!/usr/bin/perl

#use Math::CDF qw(:all);

#my @g== (10,25,50,75,100,250,500,1000,2000);
my @g== (10,25,50,75,100,250);

%hash_mo = ();
%hash_saw = ();
%hash_mew = ();
%hash_topsis = ();
%hash_mew_it = ();
%hash_saw_it = ();
%hash_topsis_it = ();
%hash_gap = ();
%hash_count = ();

%hash_diff_mo = ();
%hash_diff_saw = ();
%hash_diff_mew = ();
%hash_diff_topsis = ();
%hash_diff_mew_it = ();
%hash_diff_saw_it = ();
%hash_diff_topsis_it = ();
%hash_diff_gap = ();
%hash_diff_count = ();

open FILE, "counts_sol";

foreach my $line (<FILE>)
{
	my @array = split ('\t',$line);
	print $line;
	
	if ($array[0]>0)
	{
		$hash_mo{$array[0]}{$array[1]}+=$array[2];
		$hash_saw{$array[0]}{$array[1]}+=$array[3];
		$hash_mew{$array[0]}{$array[1]}+=$array[4];
        $hash_topsis{$array[0]}{$array[1]}+=$array[5];
        $hash_saw_it{$array[0]}{$array[1]}+=$array[6];
        $hash_mew_it{$array[0]}{$array[1]}+=$array[7];
        $hash_topsis_it{$array[0]}{$array[1]}+=$array[8];
        $hash_gap{$array[0]}{$array[1]}+=$array[9];
		$hash_count{$array[0]}{$array[1]}++;
	}
}


close FILE;


#divide by n

foreach my $key (sort {$a<=>$b} keys %hash_count)
{
	open OUT, ">nb_sol_avg_$key";

	foreach my $chrom (sort {$a<=>$b} keys %{$hash_count{$key}})
	{
		my $avg_mo = $hash_mo{$key}{$chrom} / $hash_count{$key}{$chrom};
		$hash_mo{$key}{$chrom} = $hash_mo{$key}{$chrom} / $hash_count{$key}{$chrom};
		my $avg_saw = $hash_saw{$key}{$chrom} / $hash_count{$key}{$chrom};
		$hash_saw{$key}{$chrom} = $hash_saw{$key}{$chrom} / $hash_count{$key}{$chrom};
		my $avg_mew = $hash_mew{$key}{$chrom} / $hash_count{$key}{$chrom};
		$hash_mew{$key}{$chrom} = $hash_mew{$key}{$chrom} / $hash_count{$key}{$chrom};
		my $avg_topsis = $hash_topsis{$key}{$chrom} / $hash_count{$key}{$chrom};
		$hash_topsis{$key}{$chrom} = $hash_topsis{$key}{$chrom} / $hash_count{$key}{$chrom};
		my $avg_saw_it = $hash_saw_it{$key}{$chrom} / $hash_count{$key}{$chrom};
		$hash_saw_it{$key}{$chrom} = $hash_saw_it{$key}{$chrom} / $hash_count{$key}{$chrom};
		my $avg_mew_it = $hash_mew_it{$key}{$chrom} / $hash_count{$key}{$chrom};
		$hash_mew_it{$key}{$chrom} = $hash_mew_it{$key}{$chrom} / $hash_count{$key}{$chrom};
		my $avg_topsis_it = $hash_topsis_it{$key}{$chrom} / $hash_count{$key}{$chrom};
		$hash_topsis_it{$key}{$chrom} = $hash_topsis_it{$key}{$chrom} / $hash_count{$key}{$chrom};
		my $avg_gap = $hash_gap{$key}{$chrom} / $hash_count{$key}{$chrom};
		$hash_gap{$key}{$chrom} = $hash_gap{$key}{$chrom} / $hash_count{$key}{$chrom};
		
		if($chrom%2==0)
		{
            my $nb_app = $chrom / 2;
			print OUT "$nb_app\t$avg_mo\t$avg_saw\t$avg_mew\t$avg_topsis\t$avg_saw_it\t$avg_mew_it\t$avg_topsis_it\t$avg_gap\n";	
		}
	}
	
	#close OUT;
}

#calculation of sum (sigma2)

open FILE, "counts_sol";

foreach my $line (<FILE>)
{
	my @array = split ('\t',$line);
	print $line;
	
	if ($array[0]>0)
	{
		$hash_diff_mo{$array[0]}{$array[1]} += ($array[2] - $hash_mo{$array[0]}{$array[1]})**2;
		$hash_diff_saw{$array[0]}{$array[1]} += ($array[3] - $hash_saw{$array[0]}{$array[1]})**2;
		$hash_diff_mew{$array[0]}{$array[1]} += ($array[4] - $hash_mew{$array[0]}{$array[1]})**2;
        $hash_diff_topsis{$array[0]}{$array[1]} += ($array[5] - $hash_topsis{$array[0]}{$array[1]})**2;
        $hash_diff_saw_it{$array[0]}{$array[1]} += ($array[6] - $hash_saw_it{$array[0]}{$array[1]})**2;
        $hash_diff_mew_it{$array[0]}{$array[1]} += ($array[7] - $hash_mew_it{$array[0]}{$array[1]})**2;
        $hash_diff_topsis_it{$array[0]}{$array[1]} += ($array[8] - $hash_topsis_it{$array[0]}{$array[1]})**2;
        $hash_diff_gap{$array[0]}{$array[1]} += ($array[9] - $hash_gap{$array[0]}{$array[1]})**2;
	}
}


close FILE;

#divide by n and square root of sigma2 to obtain sigma

foreach my $key (sort {$a<=>$b} keys %hash_count)
{
	open OUT, ">nb_sol_avg_confidence_$key";

	foreach my $chrom (sort {$a<=>$b} keys %{$hash_count{$key}})
	{	
		#equivalent to do (1.96 * sigma) / sqrt(n). This is confidence interval assuming normal distribution and 95% confidence
		my $avg_mo_conf_interval =  1.96 * sqrt($hash_diff_mo{$key}{$chrom}) / $hash_count{$key}{$chrom}; 
		my $avg_mo = $hash_mo{$key}{$chrom};
		my $avg_mo_sup = $hash_mo{$key}{$chrom} + $avg_mo_conf_interval;
		my $avg_mo_inf = $hash_mo{$key}{$chrom} - $avg_mo_conf_interval;
		my $avg_saw_conf_interval = 1.96 * sqrt($hash_diff_saw{$key}{$chrom}) / $hash_count{$key}{$chrom};
		my $avg_saw = $hash_saw{$key}{$chrom};
		my $avg_saw_sup = $hash_saw{$key}{$chrom} + $avg_saw_conf_interval;
		my $avg_saw_inf = $hash_saw{$key}{$chrom} - $avg_saw_conf_interval;
		my $avg_mew_conf_interval = 1.96 * sqrt($hash_diff_mew{$key}{$chrom}) / $hash_count{$key}{$chrom};
		my $avg_mew = $hash_mew{$key}{$chrom};
		my $avg_mew_sup = $hash_mew{$key}{$chrom} + $avg_mew_conf_interval;
		my $avg_mew_inf = $hash_mew{$key}{$chrom} - $avg_mew_conf_interval;
		my $avg_topsis_conf_interval = 1.96 * sqrt($hash_diff_topsis{$key}{$chrom}) / $hash_count{$key}{$chrom};
		my $avg_topsis = $hash_topsis{$key}{$chrom};
		my $avg_topsis_sup = $hash_topsis{$key}{$chrom} + $avg_topsis_conf_interval;
		my $avg_topsis_inf = $hash_topsis{$key}{$chrom} - $avg_topsis_conf_interval;
		my $avg_saw_it_conf_interval = 1.96 * sqrt($hash_diff_saw_it{$key}{$chrom}) / $hash_count{$key}{$chrom};
		my $avg_saw_it = $hash_saw_it{$key}{$chrom};
		my $avg_saw_it_sup = $hash_saw_it{$key}{$chrom} + $avg_saw_it_conf_interval;
		my $avg_saw_it_inf = $hash_saw_it{$key}{$chrom} - $avg_saw_it_conf_interval;
		my $avg_mew_it_conf_interval = 1.96 * sqrt($hash_diff_mew_it{$key}{$chrom}) / $hash_count{$key}{$chrom};
		my $avg_mew_it = $hash_mew_it{$key}{$chrom};
		my $avg_mew_it_sup = $hash_mew_it{$key}{$chrom} + $avg_mew_it_conf_interval;
		my $avg_mew_it_inf = $hash_mew_it{$key}{$chrom} - $avg_mew_it_conf_interval;
		my $avg_topsis_it_conf_interval = 1.96 * sqrt($hash_diff_topsis_it{$key}{$chrom}) / $hash_count{$key}{$chrom};
		my $avg_topsis_it = $hash_topsis_it{$key}{$chrom};
		my $avg_topsis_it_sup = $hash_topsis_it{$key}{$chrom} + $avg_topsis_it_conf_interval;
		my $avg_topsis_it_inf = $hash_topsis_it{$key}{$chrom} - $avg_topsis_it_conf_interval;
		my $avg_gap_conf_interval = 1.96 * sqrt($hash_diff_gap{$key}{$chrom}) / $hash_count{$key}{$chrom};
		my $avg_gap = $hash_gap{$key}{$chrom};
		my $avg_gap_sup = $hash_gap{$key}{$chrom} + $avg_gap_conf_interval;
		my $avg_gap_inf = $hash_gap{$key}{$chrom} - $avg_gap_conf_interval;
		
		
		if($chrom%2==0)
		{
            my $nb_app = $chrom / 2;
			print OUT "$nb_app\t$avg_mo\t$avg_saw\t$avg_mew\t$avg_topsis\t$avg_saw_it\t$avg_mew_it\t$avg_topsis_it\t$avg_gap\t$avg_mo_inf\t$avg_saw_inf\t$avg_mew_inf\t$avg_topsis_inf\t$avg_saw_it_inf\t$avg_mew_it_inf\t$avg_topsis_it_inf\t$avg_gap_inf\t$avg_mo_sup\t$avg_saw_sup\t$avg_mew_sup\t$avg_topsis_sup\t$avg_saw_it_sup\t$avg_mew_it_sup\t$avg_topsis_it_sup\t$avg_gap_sup\n";	
		}
	}
	
	close OUT;
}


open PLOT_AVG, ">plot_num_sol_avg.gnuplot";

print PLOT_AVG "set terminal postscript color enhanced\n";
print PLOT_AVG "set output \"plot_num_sol_avg.eps\"\n";
print PLOT_AVG "set xlabel \"Number of Applications\"\n";
print PLOT_AVG "set ylabel \"Number of Solutions\"\n";
print PLOT_AVG "set colorsequence classic\n";
print PLOT_AVG "set key below\n";
print PLOT_AVG "set grid\n";
print PLOT_AVG "set xrange [0:13]\n";
print PLOT_AVG "plot \"nb_sol_avg_10\" u 1:2 with points ps 1.5 lw 3 lc 1 title \"g=10\", \"nb_sol_avg_25\" u 1:2 with points ps 1.5 lw 3 lc 1 title \"g=25\", \"nb_sol_avg_50\" u 1:2 with points ps 1.5 lw 3 lc 1 title \"g=50\", \"nb_sol_avg_100\" u 1:2 with points ps 1.5 lw 3 lc 1 title \"g=100\", \"nb_sol_avg_250\" u 1:2 with points ps 1.5 lw 3 lc 1 title \"g=250\", \"nb_sol_avg_10\" u 1:3 with points ps 1.5 lw 3 lc 3 title \"SAW\",\"nb_sol_avg_10\" u 1:4 with points ps 1.5 lw 3 lc 3 title \"MEW\", \"nb_sol_avg_10\" u 1:5 with points ps 1.5 lw 3 lc 3 title \"TOPSIS\", \"nb_sol_avg_10\" u 1:6 with points ps 1.5 lw 3 lc 9 title \"SAW p/ APP\", \"nb_sol_avg_10\" u 1:7 with points ps 1.5 lw 3 lc 9 title \"MEW p/ APP\", \"nb_sol_avg_10\" u 1:8 with points ps 1.5 lw 3 lc 9 title \"TOPSIS p/ APP\", \"nb_sol_avg_10\" u 1:9 with points ps 1.5 lw 3 lc 5 title \"GAP\"";

close PLOT_AVG;

open PLOT_AVG, ">plot_num_sol_avg_confidence.gnuplot";

print PLOT_AVG "set terminal postscript color enhanced\n";
print PLOT_AVG "set output \"plot_num_sol_avg_confidence.eps\"\n";
print PLOT_AVG "set xlabel \"Number of Applications\"\n";
print PLOT_AVG "set ylabel \"Number of Solutions\"\n";
print PLOT_AVG "set colorsequence classic\n";
print PLOT_AVG "set key below\n";
print PLOT_AVG "set grid\n";
print PLOT_AVG "set xrange [0:13]\n";
print PLOT_AVG "plot \"nb_sol_avg_confidence_250\" u 1:2:10:18 with yerror ps 1.5 lw 3 lc 1 title \"g=250\", \"nb_sol_avg_confidence_10\" u 1:4:12:20 with yerror ps 1.5 lw 3 lc 3 title \"MEW\", \"nb_sol_avg_confidence_10\" u 1:6:14:22 with yerror ps 1.5 lw 3 lc 9 title \"SAW p/ APP\", \"nb_sol_avg_confidence_10\" u 1:9:17:25 with yerror ps 1.5 lw 3 lc 5 title \"GAP\"";

close PLOT_AVG;

my $result = `gnuplot plot_num_sol_avg.gnuplot`;
my $result = `gnuplot plot_num_sol_avg_confidence.gnuplot`;

