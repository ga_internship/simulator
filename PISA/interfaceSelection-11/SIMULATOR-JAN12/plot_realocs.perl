#!/usr/bin/perl

use POSIX;
use strict;

open INPUT,"../reallocs";

open OUTPUT_10,">reallocs_10";
open OUTPUT_25,">reallocs_25";
open OUTPUT_50,">reallocs_50";
open OUTPUT_75,">reallocs_75";
open OUTPUT_100,">reallocs_100";
open OUTPUT_250,">reallocs_250";
#open OUTPUT_500,">reallocs_500";
#open OUTPUT_1000,">reallocs_1000";
#open OUTPUT_2000,">reallocs_2000";

my $id = -1;

foreach my $line (<INPUT>)
{
	my @array_line = split('\t',$line);

	my $time = $array_line[0];
	my $gen = $array_line[1];
	my $saw = $array_line[2];
	my $mew = $array_line[3];
	my $topsis = $array_line[4];
	my $saw_iter = $array_line[5];
	my $mew_iter = $array_line[6];
	my $topsis_iter = $array_line[7];
	my $gap = $array_line[8];
	my $ga = $array_line[9];

	if ($gen == 10)
	{
		$id++;
		print OUTPUT_10 "$id	$saw	$mew	$topsis	$saw_iter	$mew_iter	$topsis_iter	$gap	$ga";
	}elsif ($gen == 25)
	{
		print OUTPUT_25 "$id	$saw	$mew	$topsis	$saw_iter	$mew_iter	$topsis_iter	$gap	$ga";
	}elsif ($gen == 50)
	{
		print OUTPUT_50 "$id	$saw	$mew	$topsis	$saw_iter	$mew_iter	$topsis_iter	$gap	$ga";
	}elsif ($gen == 75)
	{
		print OUTPUT_75 "$id	$saw	$mew	$topsis	$saw_iter	$mew_iter	$topsis_iter	$gap	$ga";
	}elsif ($gen == 100)
	{
		print OUTPUT_100 "$id	$saw	$mew	$topsis	$saw_iter	$mew_iter	$topsis_iter	$gap	$ga";
	}elsif ($gen == 250)
	{
		print OUTPUT_250 "$id	$saw	$mew	$topsis	$saw_iter	$mew_iter	$topsis_iter	$gap	$ga";
	}#elsif ($gen == 500)
	#{
		#print OUTPUT_500 "$id	$saw	$mew	$ga";
	#}elsif ($gen == 1000)
	#{
		#print OUTPUT_1000 "$id	$saw	$mew	$ga";
	#}elsif ($gen == 2000)
	#{
		#print OUTPUT_2000 "$id	$saw	$mew	$ga";
	#}

} 

close(OUTPUT_10);
close(OUTPUT_25);
close(OUTPUT_50);
close(OUTPUT_75);
close(OUTPUT_100);
close(OUTPUT_250);
#close(OUTPUT_500);
#close(OUTPUT_1000);
#close(OUTPUT_2000);

open PLOT_MINS, ">plot_reallocs.gnuplot";

print PLOT_MINS "set terminal postscript color enhanced\n";
print PLOT_MINS "set output \"plot_reallocs.eps\"\n";
print PLOT_MINS "set xlabel \"Event\"\n";
print PLOT_MINS "set ylabel \"Total number of reallocations\"\n";
print PLOT_MINS "set key below\n";
print PLOT_MINS "plot \"reallocs_10\" u 1:9 with lines lw 4 title \"Gen=10\", \"reallocs_25\" u 1:9 with lines lw 4 title \"Gen=25\", \"reallocs_50\" u 1:9 with lines lw 4 title \"Gen=50\", \"reallocs_75\" u 1:9 with lines lw 4 title \"Gen=75\", \"reallocs_100\" u 1:9 with lines lw 4 title \"Gen=100\",\"reallocs_250\" u 1:9 with lines lw 4 title \"Gen=250\",\"reallocs_10\" u 1:2 with lines lw 2 lc 4 title \"MADM-SAW\",\"reallocs_10\" u 1:3 with lines lw 2 lc 3 title \"MADM-MEW\",\"reallocs_10\" u 1:4 with lines lw 2 title \"MADM-TOPSIS\",\"reallocs_10\" u 1:5 with lines lw 2 title \"MADM-SAW-ITER\",\"reallocs_10\" u 1:6 with lines lw 2 title \"MADM-MEW-ITER\",\"reallocs_10\" u 1:7 with lines lw 2 title \"MADM-TOPSIS-ITER\",\"reallocs_10\" u 1:8 with lines lw 2 title \"GAP\"";

#this was taken from the middle of what is up from this text
#\"reallocs_500\" u 1:4 with lines lw 4 title \"Gen=500\",\"reallocs_1000\" u 1:4 with lines lw 4 title \"Gen=1000\",\"reallocs_2000\" u 1:4 with lines lw 4 title \"Gen=2000\",
close PLOT_MINS;

my $result = `gnuplot plot_reallocs.gnuplot`;
