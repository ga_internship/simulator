#!/bin/bash
#
# nautilus-rotateRight-pdf

pdftk "$*" cat 1-endR output "rot$*"
rm "$*"
mv -T "rot$*" "$*"
