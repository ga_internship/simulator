#!/usr/bin/perl

open DISTANCE,"distance";
open AVGDIST, ">avg_distance";

my %hash = ();
my %count = ();

foreach my $line (<DISTANCE>)
{
	my @array = split('\t',$line);

	$hash{$array[0]} += $array[1];
	$count{$array[0]} ++ ;

}

close DISTANCE;

my %avg = ();

foreach my $key (sort keys %hash)
{
	my $avg = $hash{$key} / $count{$key};
	$avg{$key} = $avg;
	#print "$key	$avg\n";
}

open DISTANCE,"distance";

my %sum = ();

foreach my $line (<DISTANCE>)
{
	my @array = split('\t',$line);
	$sum{$array[0]} += ($array[1] - $avg{$array[0]})**2; 	
}

foreach my $key (sort {$a<=>$b} keys %hash)
{
	my $avg = $hash{$key} / $count{$key};
	#$avg{$key} = $avg;
	my $std = ( $sum{$key} / $count{$key} )**(0.5);
	my $CoV = $std / $avg;
	print AVGDIST "$key	$avg	$std	$CoV\n";
}
