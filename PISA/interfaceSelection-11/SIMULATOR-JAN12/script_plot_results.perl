#!/usr/bin/perl

my $result;

$result = `perl num_sol_avg.perl`; #number of solutions
$result = `perl perl_distance.pl`; #distance to MEW and SAW optimal solutions
$result = `perl time.perl`; #Calculation delay for GA and MADM
$result = `gnuplot plot_delay_avg.gnuplot`; # plot for calculation delay
$result = `perl perl_avg_realloc_all.perl`; # average reallocations
$result = `gnuplot plot_avg_reallocs_all.gnuplot`; # plot for average reallocations

$result =`epstopdf plot_num_sol_avg.eps`;
$result =`epstopdf plot_diff_avg_saw.eps`;
$result =`epstopdf plot_diff_avg_mew.eps`;
$result =`epstopdf plot_avg_reallocs_all.eps`;
$result =`epstopdf delay_avg.eps`;

$result =`./rotatePDFright.sh plot_num_sol_avg.pdf`;
$result =`./rotatePDFright.sh plot_diff_avg_saw.pdf`;
$result =`./rotatePDFright.sh plot_diff_avg_mew.pdf`;
$result =`./rotatePDFright.sh plot_avg_reallocs_all.pdf`;
$result =`./rotatePDFright.sh delay_avg.pdf`;
