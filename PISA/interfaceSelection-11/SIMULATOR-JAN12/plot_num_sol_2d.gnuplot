set terminal postscript color enhanced
set output "plot_num_sol_2d.eps"
set xlabel "Chromosomes"
set ylabel "Number of Different Solutions"
plot 'counts_sol' u 2:3 title "MO", 'counts_sol' u 2:4 title "MADM" 
