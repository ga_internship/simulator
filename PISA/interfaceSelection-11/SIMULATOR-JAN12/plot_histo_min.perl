#!/usr/bin/perl

open RATIO_DIST,"radio_dist_min";
open AVGHIST, ">avg_hist";
open MINHIST, ">min_hist";
 
my %hash_min = ();
my %hash_avg = ();

my $min_min = 9999999999;
my $min_avg = 9999999999;

my $min_min_pos = 0;
my $min_avg_pos = 0;

#for (my $i = 0; $i < 10; $i++)
foreach my $line (<RATIO_DIST>)	
{
	my @line_array = split ('\t', $line);

	if ($line_array[0] eq '10')
	{
		print "START\n";
		$min_min = 9999999999;
		$min_avg = 9999999999;
		
	} 

	if ($line_array[2] < $min_avg)
	{
		$min_avg = $line_array[2];
		$min_avg_pos = $line_array[0];
	}

	if ($line_array[3] < $min_min)
	{
		$min_min = $line_array[3];
		$min_min_pos = $line_array[0];
	}

	if ($line_array[0] eq '2000')
	{
		print "END	$min_min	$min_min_pos	$min_avg	$min_avg_pos\n";

		$hash_min{$min_min_pos}++;
		$hash_avg{$min_avg_pos}++;

	}
}

my $sum_min = 0;
foreach my $key (sort {$a<=>$b} keys %hash_min)
{
	#print OUTMIN "$key	$hash_min{$key}\n";
	$sum_min += $hash_min{$key};
}

my $sum_avg = 0;
foreach my $key (sort {$a<=>$b} keys %hash_avg)
{
	#print OUTAVG"$key	$hash_avg{$key}\n";
	$sum_avg += $hash_avg{$key};
}

my $cdf = 0;
open OUTMIN, ">out_min";
foreach my $key (sort {$a<=>$b} keys %hash_min)
{
	$cdf += $hash_min{$key}; 
	my $ratio = $cdf / $sum_min;
	print OUTMIN "$key	$hash_min{$key}	$ratio\n";

}

$cdf = 0;
open OUTAVG, ">out_avg";
foreach my $key (sort {$a<=>$b} keys %hash_avg)
{
	$cdf += $hash_avg{$key}; 
	my $ratio = $cdf / $sum_avg;
	print OUTAVG"$key	$hash_avg{$key}	$ratio\n";
}
close OUTMIN;
close OUTAVG;

open PLOT_MINS, ">plot_mins.gnuplot";

print PLOT_MINS "set terminal postscript color enhanced\n";
print PLOT_MINS "set output \"plot_mins.eps\"\n";
print PLOT_MINS "set xlabel \"Generations\"\n";
print PLOT_MINS "set ylabel \"Histogram\"\n";
print PLOT_MINS "set log x\n";
print PLOT_MINS "set log y\n";
print PLOT_MINS "set y2tics\n";
print PLOT_MINS "set ytics nomirror\n";
print PLOT_MINS "set key below\n";
print PLOT_MINS "set grid y2\n";
print PLOT_MINS "set xrange [8:3000]\n";
print PLOT_MINS "plot \"out_min\" u 1:2 with impulses lw 4 lt 1 lc 1 title \"Min Distance\", \"out_min\" u 1:3 with linespoints lw 4 lt 1 lc 1title \"Min Distance (CDF)\" axes x1y2, \"out_avg\" u 1:2 with impulses lw 4 lt 2 lc 2 title \"Min Average Distance\", \"out_avg\" u 1:3 with linespoints lw 4 lt 2 lc 2 title \"Min Average Distance (CDF)\" axes x1y2";

close PLOT_MINS;

my $result = `gnuplot plot_mins.gnuplot`;


