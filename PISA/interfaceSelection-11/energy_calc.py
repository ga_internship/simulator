# Here we calculate the average and total energy consumption compared with several metrics of bandwith using the different choices of interfaces flows combinations.

import numpy
import os
from subprocess import call

algorithms = ['gap','madm','madm_mew','madm_saw_iter','madm_mew_iter','output']
number_of_generations = [10,25,50,75,100,250]
number_of_generations_str = [str(gen) for gen in number_of_generations]
number_of_populations = [20,60,100]



text_file = open("flowgraph", "r")
lines = text_file.read().split('\n')

fp_energy_bdw_plot = open("energy_bdw_plot.txt","w")

for i in range(len(lines)):
	lines[i] = lines[i].split(' ')
	if lines[i][0] <> '':
		del lines[i][1]
		del lines[i][2]
		#lines[i][0] = float(lines[i][0])
		lines[i][1] = float(lines[i][1])
		lines[i][2] = int(lines[i][2])
		lines[i][3] = int(lines[i][3])
	else:
		del lines[i]
text_file.close()
flows = lines
#del lines[0][1]
for flow in flows:
	print flow
	
text_files = []
text_files.append(open("ifacegraph_0", "r"))
text_files.append(open("ifacegraph_1", "r"))
text_files.append(open("ifacegraph_2", "r"))
text_files.append(open("ifacegraph_3", "r"))
ifaces = []
for text_file in text_files:
	lines = text_file.read().split('\n')
	for i in range(len(lines)):
		lines[i] = lines[i].split(' ')
		if lines[i][0] <> '':
			#del lines[i][1]
			#del lines[i][2]
			lines[i][0] = float(lines[i][0])
			lines[i][1] = int(lines[i][1])
			#lines[i][2] = int(lines[i][2])
			#lines[i][3] = int(lines[i][3])
		#else:
			#del lines[i]
	del lines[-1]
	text_file.close()
	ifaces.append(lines)
for iface in ifaces:
	for status in iface:
		print status


times = [flows[0][0]]
for i in range(1,len(flows)):
	if flows[i][3] <> flows[i-1][3]:
		times.append(flows[i][0])

for time in times:
	print time		


decisions = {}

#for dirname, dirnames, filenames in os.walk('./SIMULATOR-JAN12/1477928327/'):
    # print path to all subdirectories first.
    #for subdirname in dirnames:
        #print(os.path.join(dirname, subdirname))

    # print path to all filenames.
    #for filename in filenames:
for time in times:
	decisions[time] = {}
	for generation in number_of_generations_str:
		decisions[time][generation] = {}
		if generation == '10':
			algorithms_if = algorithms
		else:
			algorithms_if = ['output']
		for algorithm in algorithms_if:
			dir_and_file_name ='./SIMULATOR-JAN12/1477928327/' + time + '-' + generation + '/' + algorithm + '_' + time + '-' + generation + '.txt'
			print dir_and_file_name
			text_file = open(dir_and_file_name)
			lines = text_file.read().split('\n')
			lines = [line.split('\t') for line in lines]
			decisions[time][generation][algorithm] = lines
			text_file.close()
			#for line in lines:
				#print line

    # Advanced usage:
    # editing the 'dirnames' list will stop os.walk() from recursing into there.
    #if '.git' in dirnames:
        # don't go into any .git directories.
        #dirnames.remove('.git')

for time in times:
	for generation in number_of_generations_str:
		if generation == '10':
			algorithms_if = algorithms
		else:
			algorithms_if = ['output']
		for algorithm in algorithms_if:
			print time,generation,algorithm
			for line in decisions[time][generation][algorithm]:
				print line
print 'end'	

#Distance to (0,0) to  choose best option

i = 0

#MADM

#This needs to be corrected to be normalized with the extreme values in order to make the comparison fair.
for time in times:
	print time
	fp_energy_bdw_plot.write(time + "\t")
	for generation in number_of_generations_str:
		for algorithm in algorithms:
			#print algorithm
			value = 0
			new_value = 0
			
			max_bw = 0.
			min_bw = 999999999999.
			max_en = 0.
			min_en = 999999999999.
			if generation == '10':
				for line in decisions[time]['10'][algorithm]:
					if line <> ['']:
						if float(line[4]) > max_bw:
							max_bw = float(line[4])
						if float(line[4]) < min_bw:
							min_bw = float(line[4])
						if float(line[2]) > max_en:
							max_en = float(line[2])
						if float(line[2]) < min_en:
							min_en = float(line[2])
						
				dif_bw = max_bw - min_bw
				dif_en = max_en - min_en
				
				if dif_bw == 0:
					dif_bw = 1
				if dif_en == 0:
					dif_en = 1
				
				#print "dif_bw",dif_bw
				#print "dif_en",dif_en
				
				for line in decisions[time]['10'][algorithm]:
					if line <> ['']:	
						new_value = numpy.sqrt(numpy.power(float(line[2])/dif_en, 2) + numpy.power(float(line[4])/dif_bw, 2))
						#print float(line[2]), float(line[4])
						if value < new_value:
							value = new_value
							bandwidth = float(line[4])
							energy = float(line[2])
				if algorithm in ['gap','madm','output']:
					print algorithm, "\t\t\tBAND\t", bandwidth,"\t\tENER\t",energy
				else:
					print algorithm, "\t\tBAND\t", bandwidth,"\t\tENER\t",energy
				fp_energy_bdw_plot.write(str(algorithm) + "\tBAND\t" + str(bandwidth) + "\t\tENER\t" + str(energy)+ "\n")

#call(["gnuplot",""])	
fp_energy_bdw_plot.close()