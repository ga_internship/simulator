"""Module with main parts of NSGA-II algorithm.
Contains main loop"""
from __future__ import print_function
from nsga2.utils import NSGA2Utils
from nsga2.population import Population

class Evolution(object):
    
    def __init__(self, problem, num_of_generations, num_of_individuals,m,first_event,previous_population,nb_apps,perc_gen_mutate):
        self.utils = NSGA2Utils(problem, num_of_individuals,m,first_event,previous_population,nb_apps,perc_gen_mutate) # m: max size of gene

        self.population = None
        self.num_of_generations = num_of_generations
        self.on_generation_finished = []
        self.num_of_individuals = num_of_individuals
        self.FEATURES = 0
        self.OBJECTIVES = 1

        #self.fitness_table = self.get_fitness_file()
        #self.broadcast_fitness_table = sc.broadcast(self.fitness_table)
        #self.broadcast_fitness_table = self.get_fitness_file()

    def get_fitness_file(self):
        PRESPONSE_I = 0
        OP_CHANNEL = 6
        NIC_CHANNEL = 7
        DELAY_J = 8
        DELAY_K = 9

        fobj = open('summary2.csv','r')
        tmp = fobj.readline()
        table = [range(16)]
        for i in fobj:
            table.append(i.split('; '))
            table[-1][PRESPONSE_I] = int(table[-1][PRESPONSE_I])
            table[-1][OP_CHANNEL] = int(table[-1][OP_CHANNEL])
            table[-1][NIC_CHANNEL] = int(table[-1][NIC_CHANNEL])
            table[-1][DELAY_J] = int(table[-1][DELAY_J])
            table[-1][DELAY_K ] = float(table[-1][DELAY_K])
            
        fobj.close()
        return table

    def register_on_new_generation(self, fun):
        self.on_generation_finished.append(fun)

    def evolve(self):
        list_pareto_front_size = []
        list_population_size = []
        self.population = self.utils.create_initial_population()
        self.utils.fast_nondominated_sort(self.population)
        for front in self.population.fronts:
            self.utils.calculate_crowding_distance(front)
        children = self.utils.create_children(self.population)
        returned_population = None 
        for i in range(self.num_of_generations):
            self.population.extend(children)
            self.utils.fast_nondominated_sort(self.population)
            new_population = Population()
            front_num = 0
            break_ = 0
            while len(new_population) + len(self.population.fronts[front_num]) <= self.num_of_individuals:
                self.utils.calculate_crowding_distance(self.population.fronts[front_num])
                new_population.extend(self.population.fronts[front_num])
                if front_num < len(self.population.fronts) - 1:
                    front_num += 1
                else:
                    break_ = 1
                    break
            if break_ == 0 :
                self.utils.calculate_crowding_distance(self.population.fronts[front_num])
                self.population.fronts[front_num] = sorted(self.population.fronts[front_num], cmp=self.utils.crowding_operator)
                new_population.extend(self.population.fronts[front_num][0:self.num_of_individuals-len(new_population)])
            else:
                self.population.fronts[front_num] = sorted(self.population.fronts[front_num], cmp=self.utils.crowding_operator)
                if self.num_of_individuals-len(new_population) > 0:
                    new_population.extend(self.population.fronts[front_num][0:self.num_of_individuals-len(new_population)])
            returned_population = self.population
            #list_pareto_front_size.append(len(returned_population.fronts[0]))
            #list_population_size.append(len(new_population.population))
            self.population = new_population
            children = self.utils.create_children(self.population)
            if len(returned_population.fronts[0]) == 0: #nasty patch
                self.population = self.utils.create_initial_population()
            for fun in self.on_generation_finished:
                fun(returned_population, i)
        #print "Pareto Front Size and Population Size"
        #print list_pareto_front_size
        #print list_population_size
    #print len(returned_population.fronts[0])
    
        #for individual in returned_population.fronts[0]:
            #print(' '.join(str(n) for n in individual[self.FEATURES]),end = " ")

            #print(float(individual[self.OBJECTIVES][0]), individual[self.OBJECTIVES][1])
        if len(returned_population.fronts[0]) >= self.num_of_individuals:
            return returned_population.fronts[0][0:self.num_of_individuals],returned_population.fronts[0][0:self.num_of_individuals]
        else:
            if len(new_population.population) <= self.num_of_individuals:
                return returned_population.fronts[0], new_population.population
            else:
                return returned_population.fronts[0], new_population.population[0:self.num_of_individuals]
