"""Module with definition of Scanning problem interface"""

from nsga2.individual import Individual
from nsga2.individual import IndividualSpark
from nsga2.problems import Problem
import random
import functools
import numpy as np

class Scanning(Problem):

    def __init__(self, scanning_definitions,n):
        self.scanning_definitions = scanning_definitions
        self.max_objectives = [None, None, None]
        self.min_objectives = [None, None, None]
        self.problem_type = None
        self.n = n # amount of genes

    def dominatesS_notEval(self, objectives2, objectives1):
        worse_than_other = objectives1[0] >= objectives2[0] and objectives1[1] <= objectives2[1] and objectives1[2] <= objectives2[2]
        better_than_other = objectives1[0] > objectives2[0] or objectives1[1] < objectives2[1] or objectives1[2] < objectives2[2]
        return worse_than_other and better_than_other

    def generateIndividualSpark(self,broadcast_fitness_table):
        features = []
        random.seed()
        for i in range(self.n):
            features.append(random.randint(0,250))
        j = 0
        
        objectives = self.calculate_objectives_Spark(features,broadcast_fitness_table)
        hash_value = self.calculate_hash(features)    

        return [features, objectives, 0, [], 0, 0, hash_value]

    def giveIndividualDomObj(self, individual):
        individual.dominates = functools.partial(self.__dominates, individual1=individual)

    def calculate_objectives(self, individual):
        individual.objectives = []
        individual.objectives.append(self.scanning_definitions.f1(individual))
        individual.objectives.append(self.scanning_definitions.f2(individual))
        for i in range(2):
            if self.min_objectives[i] is None or individual.objectives[i] < self.min_objectives[i]:
                self.min_objectives[i] = individual.objectives[i]
            if self.max_objectives[i] is None or individual.objectives[i] > self.max_objectives[i]:
                self.max_objectives[i] = individual.objectives[i]

    def calculate_objectives_Spark(self, features,broadcast_fitness_table):
        return self.scanning_definitions.fitness_scanning(features,broadcast_fitness_table)

    def define_min_maxS(self,minS,maxS):
        for i in range(3):
            if self.min_objectives[i] is None or self.min_objectives[i] > minS[i]:
                self.min_objectives[i] = minS[i]
            if self.max_objectives[i] is None or self.max_objectives[i] < maxS[i]:
                self.max_objectives[i] = maxS[i]

    def define_min_maxS2(self,minS,maxS):
        for i in range(3):
            self.min_objectives[i] = minS[i]
            self.max_objectives[i] = maxS[i]

    def calculate_hash(self, features):        
        features_for_hash = []
        j = 0
        while j < len(features):
            features_for_hash.append(features[j])
            if  features[j] == 0:
                features_for_hash.append(0) 
            else: 
                features_for_hash.append(features[j+1])
            j += 2 
        features_for_hash2 = str(features_for_hash)
        hash_value = hash(features_for_hash2)    
        return hash_value
