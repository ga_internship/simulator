"""NSGA-II related functions"""

import functools
from nsga2.population import Population
import random
import numpy as np
from nsga2.individual import Individual
from nsga2.problems.scanning import Scanning
import itertools
from itertools import chain, imap

class NSGA2Utils(object):
    
    def __init__(self, problem, num_of_individuals,m,first_event,previous_population,nb_apps,perc_gen_mutate, mutation_strength=2, num_of_genes_to_mutate=1, num_of_tour_particips=2):
        self.FEATURES = 0
        self.OBJECTIVES = 1
        self.DOMINATION_COUNT = 2
        self.DOMINATED_SOLUTIONS = 3
        self.RANK = 4
        self.CROWDING_DISTANCE = 5
        self.OBJECTIVE1 = 0
        self.OBJECTIVE2 = 1
        self.OBJECTIVE3 = 2
        self.HASH = 6
        
        self.m = m #max value of gene
        self.first_event = first_event
        self.previous_population = previous_population
        self.problem = problem
        self.num_of_individuals = num_of_individuals
        self.mutation_strength = mutation_strength
        self.number_of_genes_to_mutate = num_of_genes_to_mutate
        #if(nb_apps<11):
            #self.number_of_genes_to_mutate = 1
        #else:
            #self.number_of_genes_to_mutate = int(perc_gen_mutate*nb_apps)
            #self.number_of_genes_to_mutate = 2
        
        self.num_of_tour_particips = num_of_tour_particips
        #self.mutation_type = mutation_type
                        
    def fast_nondominated_sort(self, population):
        population.fronts = []
        population.fronts.append([])
        self.problem.define_min_maxS2([9e12, 9e12, 9e12],[0,0,0])

        for individual in population:
            if individual[self.HASH] == 0:
                individual[self.DOMINATED_SOLUTIONS] = []
                continue
            self.problem.define_min_maxS(individual[self.OBJECTIVES])
            #print individual[self.HASH]
            individual[self.DOMINATION_COUNT] = 0
            individual[self.DOMINATED_SOLUTIONS] = []
            for other_individual in population:
                if (individual <> other_individual and individual[self.HASH] == other_individual[self.HASH]) or other_individual[self.HASH] == 0:
                    other_individual[self.HASH] = 0
                    other_individual[self.DOMINATED_SOLUTIONS] = []
                    continue
                        

                if self.dominatesS_notEval(other_individual[self.OBJECTIVES], individual[self.OBJECTIVES]):
                    individual[self.DOMINATED_SOLUTIONS].append(other_individual)
                elif self.dominatesS_notEval(individual[self.OBJECTIVES], other_individual[self.OBJECTIVES]):
                    individual[self.DOMINATION_COUNT] += 1
            if individual[self.DOMINATION_COUNT] == 0: 
                population.fronts[0].append(individual)
                individual[self.RANK] = 0 
        i = 0
        while len(population.fronts[i]) > 0:
            temp = []
            for individual in population.fronts[i]:
                if individual[self.HASH] == 0:
                    individual[self.DOMINATED_SOLUTIONS] = []
                    continue
                for other_individual in individual[self.DOMINATED_SOLUTIONS]:
                    if other_individual[self.HASH] == 0:
                        other_individual[self.DOMINATED_SOLUTIONS] = []
                        continue
                    other_individual[self.DOMINATION_COUNT] -= 1
                    if other_individual[self.DOMINATION_COUNT] == 0:
                        other_individual[self.RANK] = i+1
                        temp.append(other_individual)
                    
            population.fronts.append(temp)
            i = i+1


    def __sort_objective(self, val1, val2, m):
        return cmp(val1[self.OBJECTIVES][m], val2[self.OBJECTIVES][m])
    
    def calculate_crowding_distance(self, front):
        if len(front) > 0:
            solutions_num = len(front)
            for individual in front:
                individual[self.CROWDING_DISTANCE] = 0
            #cant_obj = len(front[0][self.OBJECTIVES])
            cant_obj = 2
            for m in range(cant_obj):
                front = sorted(front, cmp=functools.partial(self.__sort_objective, m=m))
                #print front
                #print front[0][self.CROWDING_DISTANCE],self.problem.max_objectives[m]
                front[0][self.CROWDING_DISTANCE] += 9e20
                front[solutions_num-1][self.CROWDING_DISTANCE] += 9e20
                for index, value in enumerate(front[0:solutions_num-1]):
                    if (self.problem.max_objectives[m] - self.problem.min_objectives[m]) <> 0:
                        front[index][self.CROWDING_DISTANCE] += (front[index+1][self.OBJECTIVES][m] - front[index-1][self.OBJECTIVES][m]) / (self.problem.max_objectives[m] - self.problem.min_objectives[m])
                    else:
                        front[index][self.CROWDING_DISTANCE] += 0
           
    def crowding_operator(self, individual, other_individual):
        if (individual[self.RANK] < other_individual[self.RANK]) or \
            ((individual[self.RANK] == other_individual[self.RANK]) and (individual[self.CROWDING_DISTANCE] > other_individual[self.CROWDING_DISTANCE])):
            return -1
        elif (individual[self.CROWDING_DISTANCE] != other_individual[self.CROWDING_DISTANCE]):
            return 1
        else:
            return 0
        
    
    def create_initial_population(self):
        population = Population()
        #population_spark = sc.parallelize()
        #for _ in range(self.num_of_individuals):
            #individual = self.problem.generateIndividualSpark(broadcast_fitness_table)
            #population.population.append(individual)
        if self.first_event or len(population.population) == 1 or len(population.population) == 0:
            population.population = map(lambda x: self.problem.generateIndividualSpark(),range(self.num_of_individuals))
        else:
            population.population = self.previous_population
            #print len(population.population)
            if len(population.population) < self.num_of_individuals-1:
                #print "error"
                population.population.extend(map(lambda x: self.problem.generateIndividualSpark(),range(self.num_of_individuals-len(population.population))))
        return population
    
    def create_children(self, population):
        #children_S = []
        #for _ in range(len(population)/2):
            #childs = self.generate_children_Spark(population, broadcast_fitness_table)
            #children_S.extend(childs)
        children_S = self.flatmap(lambda x: self.generate_children_Spark(population),range(len(population)/2))
        #children_S = [item for sublist in childs for item in sublist]
        
        #print "ok"
        
        return children_S

    def generate_children_Spark(self, population):
        parent1 = self.__tournament(population)
        parent2 = parent1
        i = 0
        while parent1[self.FEATURES] == parent2[self.FEATURES]:
            parent2 = self.__tournament(population)
            i += 1
            if i == 10:
                break
        #print parent1,parent2
        #print population.population
        child1, child2 = self.__crossover(parent1, parent2)
        #print child1,child2
        self.__mutate(child1)
        self.__mutate(child2)
        child1[self.OBJECTIVES] = self.problem.calculate_objectives_Spark(child1[self.FEATURES])
        child2[self.OBJECTIVES] = self.problem.calculate_objectives_Spark(child2[self.FEATURES])
        hash_value1 =  self.problem.calculate_hash(child1[self.FEATURES])
        hash_value2 =  self.problem.calculate_hash(child2[self.FEATURES])
        return [[child1[self.FEATURES], child1[self.OBJECTIVES], 0, [], 0, 0, hash_value1] , [child2[self.FEATURES], child2[self.OBJECTIVES], 0, [], 0, 0, hash_value2]]

    def __crossover(self, individual1, individual2):
        child1 = self.problem.generateIndividualSpark()
        child2 = self.problem.generateIndividualSpark()
        genes_indexes = range(len(child1[self.FEATURES]))
        random.seed()
        half_genes_indexes = random.sample(genes_indexes, int(len(child1[self.FEATURES])/2))
        for i in genes_indexes:
            if i in half_genes_indexes:
                child1[self.FEATURES][i] = individual2[self.FEATURES][i]
                child2[self.FEATURES][i] = individual1[self.FEATURES][i]
            else:
                child1[self.FEATURES][i] = individual1[self.FEATURES][i]
                child2[self.FEATURES][i] = individual2[self.FEATURES][i]
        return child1, child2

    def __mutate(self, child):
#TODO change uniform for gaussian in mutation?
        #mutation_type: could be discrete 0, or...
        #print child
        genes_to_mutate = random.sample(range(0, len(child[self.FEATURES])), self.number_of_genes_to_mutate)
        random.seed()
        for gene in genes_to_mutate:
            #child[self.FEATURES][gene] = int(child[self.FEATURES][gene] - self.mutation_strength/2 + random.random() * self.mutation_strength)
            child[self.FEATURES][gene] = child[self.FEATURES][gene] + random.randint(-self.mutation_strength,self.mutation_strength)
            if child[self.FEATURES][gene] < 0:
                child[self.FEATURES][gene] += self.m + 1
            elif child[self.FEATURES][gene] > self.m:
                child[self.FEATURES][gene] -= self.m + 1
        
    def __tournament(self, population):
        participants = random.sample(population, self.num_of_tour_particips)
        best = None
        for participant in participants:
            if best is None or self.crowding_operator(participant, best) == 1:
                best = participant
        #print best
        return best

    def dominatesS_notEval(self, objectives2, objectives1):

        worse_than_other = objectives1[0] <= objectives2[0] and objectives1[1] <= objectives2[1] and objectives1[2] <= objectives2[2]
        better_than_other = objectives1[0] < objectives2[0] or objectives1[1] < objectives2[1] or objectives1[2] < objectives2[2]
        return worse_than_other and better_than_other

    def flatmap(self,f, items):
        return chain.from_iterable(imap(f, items))
